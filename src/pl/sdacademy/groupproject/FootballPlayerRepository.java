package pl.sdacademy.groupproject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FootballPlayerRepository {

    private List<FootballPlayer> players;

    public FootballPlayerRepository(String fileName) {
        Path filePath = Paths.get(fileName);
        players = new ArrayList<>();
        try {
            Files.lines(filePath).forEach(l -> players.add(createPlayerFromFileLine(l)));
        } catch (IOException e) {
            throw new RuntimeException("Błąd odczytu piłkarzy z pliku");
        }
    }

    private FootballPlayer createPlayerFromFileLine(String fileLine) {
        String[] lineValues = fileLine.split(",");

        int id = Integer.parseInt(lineValues[0]);
        String name = lineValues[1];
        String surname = lineValues[2];
        int shooting = Integer.parseInt(lineValues[3]);
        int speed = Integer.parseInt(lineValues[4]);

        return new FootballPlayer(id, name, surname, shooting, speed);
    }
}
