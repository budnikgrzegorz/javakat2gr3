package pl.sdacademy.groupproject;

import java.util.Scanner;

public abstract class AbstractUI<T> {

    private T entity;
    private Scanner scanner;

    public AbstractUI(Scanner scanner, T entity) {
        this.scanner = scanner;
        this.entity = entity;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public T getEntity() {
        return entity;
    }

    protected String readString(String question) {
        System.out.println(question);
        return scanner.nextLine();
    }

    protected int readInt(String question) {
        System.out.println(question);
        return scanner.nextInt();
    }

    public abstract void run();

}
