package pl.sdacademy.groupproject;


import java.util.InputMismatchException;
import java.util.Scanner;

public class FootballPlayerUI extends AbstractUI<FootballPlayer> {


    public FootballPlayerUI(Scanner scanner, FootballPlayer player) {
        super(scanner, player);
    }


    public void run() {
        String choice = "";
        while (!choice.equals("koniec")) {
            choice = readString("co chcesz zrobić?: wypisz dane pilkarza (wypisz), " +
                    "edycja imienia (imie), edycja nazwiska (nazwisko), edycja strzelania (strzal), " +
                    "edycja szybkosci (szybkosc), koniec (koniec)");
            switch (choice) {
                case "wypisz":
                    print();
                    break;
                case "imie":
                    setName();
                    break;
                case "nazwisko":
                    setSurname();
                    break;
                case "strzal":
                    setShooting();
                    break;
                case "szybkosc":
                    setSpeed();
                    break;
                case "koniec":
                    System.out.println("zakończono edycję piłkarza.");
                    break;
                default:
                    System.out.println("nie wybrano dostępnej komendy");
            }

        }

    }

    private void setSpeed() {

        try {
            getEntity().setSpeed(readInt("Podaj szybkość piłkarza: "));
        } catch (InputMismatchException e) {
            System.out.println("podano nieprawidłową wartość");
        } finally {
            getScanner().nextLine();
        }
    }

    private void setShooting() {
        try {
            getEntity().setShooting(readInt("Podaj szybkość piłkarza: "));
        } catch (InputMismatchException e) {
            System.out.println("podano nieprawidłową wartość");
        } finally {
            getScanner().nextLine();
        }
    }

    private void setSurname() {
        getEntity().setSurname(readString("Podaj nazwisko piłkarza: "));
    }

    private void print() {
        System.out.println(getEntity());
    }

    private void setName() {
        getEntity().setName(readString("Podaj imię piłkarza"));
    }
}
