package pl.sdacademy.groupproject;

/**
 * Created by RENT on 2017-07-13.
 */
public class FootballPlayer extends Person {
    private int shooting;
    private int speed;

    public FootballPlayer(Integer id, String name, String surname, int shooting, int speed) {
        super(id, name, surname);
        this.shooting = shooting;
        this.speed = speed;
    }

    public FootballPlayer(String name, String surname, int shooting, int speed) {
        super(name, surname);
        this.shooting = shooting;
        this.speed = speed;

    }

    //konstruktor kopiujący - za parametr przyjmuje obiekt swojego typu i klonuje go;
    public FootballPlayer(FootballPlayer player){
        super(player.getId(), player.getName(), player.getSurname());
        this.shooting = player.getShooting();
        this.speed = player.getSpeed();
    }

    public int getShooting() {
        return shooting;
    }

    public int getSpeed() {
        return speed;
    }

    public void setShooting(int shooting) {
        this.shooting = shooting;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String toString(){
        return getName() + " " + getSurname() + " // Umiejętności: strzelanie - " + getShooting()+ ", szybkość - " + getSpeed();
    }
}
