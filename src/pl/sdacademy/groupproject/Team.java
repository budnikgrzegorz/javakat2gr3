package pl.sdacademy.groupproject;

import java.time.LocalDate;
import java.util.List;

public class Team extends AbstractEntity{
    private String name;
    private LocalDate foundationDate;
    private List<FootballPlayer> players;


    public Team(Integer id, String name, LocalDate foundationDate) {
        super(id);
        this.name = name;
        this.foundationDate = foundationDate;
    }

    public Team(String name, LocalDate foundationDate) {
        this.name = name;
        this.foundationDate = foundationDate;
    }

    public List<FootballPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<FootballPlayer> players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public LocalDate getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(int year, int month, int day) {
        foundationDate = LocalDate.of(year, month, day);
    }
}
